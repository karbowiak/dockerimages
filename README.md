# Docker Images

These images are primarily ment for use with Devspace, however they will work for other purposes as well.

They are built ontop of Ubuntu 20.04, using Ondrej's PHP and Nginx PPA to get the latest version available

- https://launchpad.net/~ondrej/+archive/ubuntu/php
- https://launchpad.net/~ondrej/+archive/ubuntu/nginx

## Beware
These images auto rebuild once a week!

## Images
There are four image types for each PHP version.

### PHP Versions
- 5.6
- 7.0
- 7.1
- 7.2
- 7.3
- 7.4
- 8.0
- 8.1

### PHP
Plain PHP image which can be gotten from: `karbowiak/docker-php:php<php version>`

### PHP + Nginx + FPM
Plain PHP with Nginx and FPM running inside which can be gotten from: `karbowiak/docker-php:php<php version>-nginx-fpm`

Replace <php version> with for example 5.6 to get: `karbowiak/docker-php:php5.6-xdebug-nginx-fpm`

### XDebug
Every image has xdebug installed, and for CLI can be called with `php-xdebug` and via nginx it's automatically enabled if you use the xdebug helper for chrome/firefox/safari/etc.
## Extras included
- MyCLI (MySQL CLi tool) https://www.mycli.net/
- ZSH with OHMyZSH (Shell making the shell experience nicer) https://ohmyz.sh/

## PHP Modules included
- readline
- cli
- common
- dev
- mbstring
- igbinary
- apcu
- imagick
- sqlite3
- yaml
- bcmath
- inotify
- mysql
- mysqlnd
- mysqli
- mongodb
- zip
- bz2
- gd
- msgpack
- raphf
- intl
- zstd
- redis
- lz4
- curl
- opcache
- xhprof
- xml
- soap
- fpm
- gmp

And then depending on if it's the xdebug build or not

- xdebug

## Extra configuration done
Regardless of the version there are some extra configuration done to the image. specifically PHP.

### CLI:
```
opcache.memory_consumption = 1024
opcache.interned_strings_buffer = 256
opcache.max_accelerated_files = 30000
opcache.validate_timestamps = 1
opcache.enable = 1
opcache.enable_cli = 1
```
### FPM:
```
opcache.memory_consumption = 1024
opcache.interned_strings_buffer = 256
opcache.max_accelerated_files = 30000
opcache.validate_timestamps = 1
opcache.enable = 1
opcache.enable_cli = 1
```
### Xdebug:
```
xdebug.client_host = 127.0.0.1
xdebug.client_port = 9003
xdebug.mode = develop,debug
```

## Modify Nginx, FPM or Supervisor
You can freely inject and/or replace configuration files when you use these containers.

To modify nginx you can simply copy your own configuration into `/etc/nginx`

To modify php and/or FPM you can simply copy in your own configuration to `/etc/php/<php version>/`

To modify supervisor (More likely add another service to it) you can copy the current `/etc/supervisor/conf.d/supervisord.conf` to your project and add whatever extra you need, or simply append your config to it.. Example config:

```
[program:custom]
command=echo 'hello world'
process_name=%(program_name)s_%(process_num)02d
numprocs=1
autostart=true
autorestart=false
startsecs=2
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
```

Beware tho, it is not the standard supervisord, it is the Golang version of it, which can be found here: https://github.com/ochinchina/supervisord

Reason was that the golang version is a single binary, where the standard supervisor is a whole bunch of python

# How to use
By default the images have no entrypoint because they're ment to be used as base images for further building with devspace.

However if you wanna run them manually you can simply use:

For nginx:
`ENTRYPOINT ["/usr/local/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]`

and if plain PHP you provide your own entrypoint

# Projector
There is a projector image with PHPStorm built into it - to have settings persist, remember to synchronize the `/root/.java` library to local, otherwise you'll have to input the token everytime you start it up from scratch.

Projector can be accessed on port 8887
