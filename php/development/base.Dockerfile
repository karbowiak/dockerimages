FROM ubuntu:20.04

# Run as root
USER root

# Non Interactive
ENV DEBIAN_FRONTEND noninteractive

# Install the PHP PPA
RUN apt update && \
    apt upgrade -y && \
    apt install -y --no-install-recommends software-properties-common gettext-base patch && \
    add-apt-repository -y ppa:ondrej/php && \
    add-apt-repository -y ppa:ondrej/nginx-mainline && \
    #add-apt-repository -y ppa:ondrej/apache2 && \
    # Install CLI tools
    apt install -y --no-install-recommends nano git iputils-ping net-tools htop unzip wget postfix && \
    sh -c "$(wget -O- https://github.com/deluan/zsh-in-docker/releases/download/v1.1.2/zsh-in-docker.sh)" -- \
    #-t https://github.com/denysdovhan/spaceship-prompt \
    #-a 'SPACESHIP_PROMPT_ADD_NEWLINE="false"' \
    #-a 'SPACESHIP_PROMPT_SEPARATE_LINE="false"' \
    -p git \
    -p dircycle \
    -p node \
    -p https://github.com/zsh-users/zsh-autosuggestions \
    -p https://github.com/zsh-users/zsh-completions \
    -p https://github.com/zsh-users/zsh-history-substring-search && \
    sed -i 's/xterm/xterm-256color/g' /root/.zshrc && \
    echo 'root:rootpw' | chpasswd && \
    apt clean && \
    apt autoclean -y && \
    apt autoremove -y && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/
