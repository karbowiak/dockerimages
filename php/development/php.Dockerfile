FROM karbowiak/docker-php:linux-base

ARG PHP_VERSION

# Run as root
USER root

# Copy in the php.ini for CLI and FPM
COPY php/development/fpm/php.fpm.ini /etc/php/${PHP_VERSION}/fpm/php.ini
COPY php/development/fpm/php.cli.ini /etc/php/${PHP_VERSION}/cli/php.ini

# Install all the PHP Packages
RUN apt update && \
    apt install -y --no-install-recommends \
    php${PHP_VERSION} \
    php${PHP_VERSION}-readline \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-common \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-igbinary \
    php${PHP_VERSION}-apcu \
    php${PHP_VERSION}-imagick \
    php${PHP_VERSION}-yaml \
    php${PHP_VERSION}-bcmath \
    php${PHP_VERSION}-mysql \
    php${PHP_VERSION}-mysqlnd \
    php${PHP_VERSION}-mysqli \
    php${PHP_VERSION}-mongodb \
    php${PHP_VERSION}-zip \
    php${PHP_VERSION}-bz2 \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-msgpack \
    php${PHP_VERSION}-intl \
    php${PHP_VERSION}-zstd \
    php${PHP_VERSION}-redis \
    php${PHP_VERSION}-lz4 \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-opcache \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-soap \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-mongodb \
    php${PHP_VERSION}-xdebug && \
    apt clean && \
    apt autoclean -y && \
    apt autoremove -y && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/ && \
    echo "#!/bin/bash" > /usr/local/bin/php-xdebug && \
    echo "mkdir -p /run/php" >> /usr/local/bin/php-xdebug && \
    echo "/usr/bin/php${PHP_VERSION} -dzend_extension=xdebug.so -dxdebug.client_host=127.0.0.1 -dxdebug.client_port=9003 -dxdebug.mode=develop,debug" >> /usr/local/bin/php-xdebug && \
    chmod +x /usr/local/bin/php-xdebug && \
    rm /etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini /etc/php/${PHP_VERSION}/fpm/conf.d/20-xdebug.ini /etc/php/${PHP_VERSION}/mods-available/xdebug.ini && \
    wget https://getcomposer.org/download/latest-2.x/composer.phar && \
    mv composer.phar /usr/local/bin/composer && \
    chmod +x /usr/local/bin/composer && \
    wget https://getcomposer.org/download/latest-1.x/composer.phar && \
    mv composer.phar /usr/local/bin/composer1 && \
    chmod +x /usr/local/bin/composer1 && \
    wget https://files.magerun.net/n98-magerun2.phar && \
    mv n98-magerun2.phar /usr/local/bin/magerun2 && \
    chmod +x /usr/local/bin/magerun2 && \
    wget https://files.magerun.net/n98-magerun.phar && \
    mv n98-magerun.phar /usr/local/bin/magerun && \
    chmod +x /usr/local/bin/magerun && \
    ln -s /usr/local/bin/magerun /usr/local/bin/m && \
    ln -s /usr/local/bin/magerun2 /usr/local/bin/m2
