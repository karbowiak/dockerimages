map $cookie_XDEBUG_SESSION $fastcgi_pass {
    default fpm;
    xdebug fpm_xdebug;
    phpstorm fpm_xdebug;
    XDEBUG_ECLIPSE fpm_xdebug;
}

upstream fpm_xdebug {
  server 127.0.0.1:9060;
}

upstream fpm {
    server 127.0.0.1:9050;
}

# Cache
proxy_busy_buffers_size 16k;
proxy_temp_file_write_size 256k;
proxy_temp_path /tmp/nginx;
proxy_cache_path /tmp/nginx/cache levels=1:2 keys_zone=STATIC:10m inactive=1d max_size=200m;

server {
    listen 8080;
    server_name _;
    set $MAGE_ROOT "/var/www";
    set $MAGE_MODE "developer";
    root $MAGE_ROOT/pub;
    access_log /proc/self/fd/2;
    error_log /proc/self/fd/2 error;
    index index.php;
    autoindex off;
    charset UTF-8;
    client_max_body_size 64M;
    error_page 404 403 = /errors/404.php;
    #add_header "X-UA-Compatible" "IE=Edge";

    location ~* ^/dev/tests/acceptance/utils($|/) {
      root $MAGE_ROOT;
      location ~ ^/dev/tests/acceptance/utils/command.php {
          fastcgi_pass   fpm_xdebug;
          fastcgi_index  index.php;
          fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
          include        fastcgi_params;
      }
    }

    # Deny access to sensitive files
    location /.user.ini {
        deny all;
    }

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }

    location /pub/ {
        location ~ ^/pub/media/(downloadable|customer|import|theme_customization/.*\.xml) {
            deny all;
        }
        alias $MAGE_ROOT/pub/;
        add_header X-Frame-Options "SAMEORIGIN";
    }

    location /static/ {
        if ($MAGE_MODE = "production") {
            expires max;
        }

        # Remove signature of the static files that is used to overcome the browser cache
        location ~ ^/static/version {
            rewrite ^/static/(version\d*/)?(.*)$ /static/$2 last;
        }

        location ~* \.(ico|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2|json)$ {
            add_header Cache-Control "public";
            add_header X-Frame-Options "SAMEORIGIN";
            expires +1y;

            if (!-f $request_filename) {
                rewrite ^/static/(version\d*/)?(.*)$ /static.php?resource=$2 last;
            }
        }
        location ~* \.(zip|gz|gzip|bz2|csv|xml)$ {
            add_header Cache-Control "no-store";
            add_header X-Frame-Options "SAMEORIGIN";
            expires    off;

            if (!-f $request_filename) {
               rewrite ^/static/(version\d*/)?(.*)$ /static.php?resource=$2 last;
            }
        }
        if (!-f $request_filename) {
            rewrite ^/static/(version\d*/)?(.*)$ /static.php?resource=$2 last;
        }
        add_header X-Frame-Options "SAMEORIGIN";
    }

    location /media/ {
        try_files $uri $uri/ /get.php$is_args$args;

        location ~ ^/media/theme_customization/.*\.xml {
            deny all;
        }

        location ~* \.(ico|jpg|jpeg|png|gif|svg|svgz|webp|avif|avifs|js|css|eot|ttf|otf|woff|woff2)$ {
            add_header Cache-Control "public";
            add_header X-Frame-Options "SAMEORIGIN";
            expires 60m;

            location ~ ^/media/catalog/product/.*$ {
                # Remove the cache part of the uri, to avoid getting a 404
                rewrite ^/media/catalog/product/(cache/.*?/)(.*)$ /media/catalog/product/$2 last;
                try_files $uri @remoteMedia;
            }
            try_files $uri $uri/ @remoteMedia;
        }

        location ~* \.(zip|gz|gzip|bz2|csv|xml)$ {
            add_header Cache-Control "no-store";
            add_header X-Frame-Options "SAMEORIGIN";
            expires    off;
            try_files $uri $uri/ /get.php$is_args$args;
        }
        add_header X-Frame-Options "SAMEORIGIN";
    }

    location /media/customer/ {
        deny all;
    }

    location /media/downloadable/ {
        deny all;
    }

    location /media/import/ {
        deny all;
    }

    location /errors/ {
        location ~* \.xml$ {
            deny all;
        }
    }

    location @remoteMedia {
        resolver 1.1.1.1 ipv6=off;
        proxy_ssl_server_name on;
        proxy_connect_timeout       10;
        proxy_send_timeout          30;
        proxy_read_timeout          30;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host ${CLIENT_MEDIA_DOMAIN};
        proxy_set_header X-Real-IP $remote_addr;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_redirect off;
        proxy_set_header Connection "upgrade";
        proxy_pass https://${CLIENT_MEDIA_DOMAIN};
        proxy_cache STATIC;
        proxy_cache_valid 200 60m;
    }

    # PHP entry point for main application
    location ~ ^/(index|get|static|errors/report|errors/404|errors/503|health_check)\.php$ {
        try_files $uri =404;
        fastcgi_pass $fastcgi_pass;
        fastcgi_buffers 1024 32k;
        fastcgi_buffer_size 256k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_keep_conn on;

        fastcgi_param  PHP_FLAG  "session.auto_start=off \n suhosin.session.cryptua=off";
        fastcgi_param  PHP_VALUE "memory_limit=768M \n max_execution_time=18000";
        fastcgi_read_timeout 600s;
        fastcgi_connect_timeout 600s;
        fastcgi_param  MAGE_MODE $MAGE_MODE;

        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include        fastcgi_params;
    }

    gzip on;
    gzip_disable "msie6";

    gzip_comp_level 6;
    gzip_min_length 1100;
    gzip_buffers 16 8k;
    gzip_proxied any;
    gzip_types
        text/plain
        text/css
        text/js
        text/xml
        text/javascript
        application/javascript
        application/x-javascript
        application/json
        application/xml
        application/xml+rss
        image/svg+xml;
    gzip_vary on;

    # Banned locations (only reached if the earlier PHP entry point regexes don't match)
    location ~* (\.php$|\.htaccess$|\.git) {
        deny all;
    }
}
